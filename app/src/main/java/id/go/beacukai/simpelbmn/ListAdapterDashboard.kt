package id.go.beacukai.simpelbmn

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import id.go.beacukai.simpelbmn.DatabaseCollection.FormObject
import java.io.File
import java.util.*

class ListAdapterDashboard(private val context: Context, private val datas: ArrayList<FormObject.MainInfo>) :
    RecyclerView.Adapter<ListAdapterDashboard.UserViewHolder>()
{

    private lateinit var storage: StorageReference
    private lateinit var arrayFotoFlag : IntArray

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder
    {
        storage = FirebaseStorage.getInstance().reference
        arrayFotoFlag = IntArray(datas.size){0}
        //val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_dashboard, parent, false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int
    {
        return datas.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int)
    {
        val data = datas[position]
        holder.name.text = data.nama
        holder.seksi.text = data.seksi
        holder.subseksi.text = data.subseksi
        holder.date.text = data.tanggal
        holder.jenis.text = data.jenis
        when(data.jenis)
        {
            "Permintaan ATK" ->
            {
                holder.atkLayout.visibility = View.VISIBLE
                holder.bmnLayout.visibility = View.GONE
                holder.barang.text = data.permintaan.nama_barang
                holder.jumlah.text = data.permintaan.jumlah_barang.toString()
            }
            "Pengaduan Masalah BMN" ->
            {
                holder.atkLayout.visibility = View.GONE
                holder.bmnLayout.visibility = View.VISIBLE
                holder.uraian.text = data.pengaduan.uraian_pengaduan
                val bitm = retrieveImage(data.key)
                if(bitm != null){
                    holder.foto.setImageBitmap(bitm)
                    arrayFotoFlag[position] = 1
                }
                else {
                    holder.foto.setImageDrawable(context.resources.getDrawable(R.drawable.image_default, context.applicationContext.theme))
                    arrayFotoFlag[position] = 0
                }
                holder.foto.setOnClickListener {
                    downloadImage(position, holder.foto)
                }
            }
            else ->
            {
                holder.atkLayout.visibility = View.GONE
                holder.bmnLayout.visibility = View.GONE
            }
        }
    }

    private fun retrieveImage(fileName :  String) : Bitmap?
    {
        val imageFolderLocation: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val imgList = imageFolderLocation?.listFiles()
        var nameTemp = ""
        if(imgList != null && imgList.isNotEmpty())
        {
            for (item in imgList){
                if(item.name.contains(fileName) && nameTemp.isEmpty())
                    nameTemp = item.name
                else if(item.name.contains(fileName) && nameTemp.isNotEmpty())
                    item.delete()
            }
        }

        val imageLink: File? = if(nameTemp.isNotEmpty()) File(imageFolderLocation, nameTemp) else null
        return if (imageLink != null && imageLink.exists())
        {
            val bitmaps = BitmapFactory.decodeFile(imageLink.path)
            bitmaps
        }
        else
        {
            null
        }
    }

    private fun downloadImage(position : Int, foto : ImageView)
    {
        if(arrayFotoFlag[position] == 0)
        {
            val progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Downloading file...")
            progressDialog.setCancelable(false)
            progressDialog.show()

            val fotoPath = storage.child("images/${datas[position].key}.jpg")
            val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            val localFile = File.createTempFile(datas[position].key, ".jpg", storageDir)
            fotoPath.getFile(localFile)
                .addOnSuccessListener {
                    progressDialog.dismiss()
                    val bitm = retrieveImage(datas[position].key)
                    if(bitm != null){
                        foto.setImageBitmap(bitm)
                        arrayFotoFlag[position] = 1
                    }
                    else {
                        foto.setImageDrawable(context.resources.getDrawable(R.drawable.image_default, context.applicationContext.theme))
                        arrayFotoFlag[position] = 0
                    }
                }
                .addOnCanceledListener {
                    progressDialog.dismiss()
                    Toast.makeText(context, "Gagal mendownload", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(context, "Gagal mendownload", Toast.LENGTH_SHORT).show()
                }
        }
    }

    class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var name = itemView.findViewById(R.id.item_form_name) as TextView
        var seksi = itemView.findViewById(R.id.item_form_seksi) as TextView
        var subseksi = itemView.findViewById(R.id.item_form_subseksi) as TextView
        var date = itemView.findViewById(R.id.item_form_date) as TextView
        var jenis = itemView.findViewById(R.id.item_form_jenis) as TextView
        var atkLayout = itemView.findViewById(R.id.item_layout_atk) as LinearLayout
        var barang = itemView.findViewById(R.id.item_form_barang_name) as TextView
        var jumlah = itemView.findViewById(R.id.item_form_jumlah_barang) as TextView
        var bmnLayout = itemView.findViewById(R.id.item_layout_bmn) as LinearLayout
        var uraian = itemView.findViewById(R.id.item_form_pengaduan) as TextView
        var foto = itemView.findViewById(R.id.item_form_foto) as ImageView
    }
}