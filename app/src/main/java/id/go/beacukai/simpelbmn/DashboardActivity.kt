package id.go.beacukai.simpelbmn

import android.app.ProgressDialog
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import id.go.beacukai.simpelbmn.DatabaseCollection.FormObject
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.apache.poi.hssf.usermodel.HSSFCellStyle
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import java.io.File
import java.util.*
import kotlin.collections.HashMap
import org.apache.poi.hssf.usermodel.HeaderFooter.file
import com.google.firebase.database.Transaction.success
import java.io.FileOutputStream
import java.io.IOException


class DashboardActivity : AppCompatActivity() {

    private lateinit var arrayData : ArrayList<FormObject.MainInfo>
    private lateinit var database: DatabaseReference
    private val excelFileName = "Rekap Data.xlsx"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        database = FirebaseDatabase.getInstance().reference.child("data")

        arrayData = ArrayList()
        initiateList()

        dashboard_refresh.setOnClickListener {
            downloadData()
        }
        dashboard_export.setOnClickListener {
            val task = ExportToExcel()
            task.execute()
        }
    }

    private fun initiateList()
    {
        if(!arrayData.isEmpty()){
            val adapter = ListAdapterDashboard(this, arrayData)
            val layoutManager = LinearLayoutManager(this)
            dashboard_listview.layoutManager = layoutManager
            dashboard_listview.adapter = adapter
            dashboard_empty_text.visibility = View.INVISIBLE
        }
        else{
            dashboard_empty_text.visibility = View.VISIBLE
        }
    }

    private fun downloadData()
    {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Download data...")
        progressDialog.setCancelable(false)
        progressDialog.show()

        database.addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onCancelled(p0: DatabaseError)
            {
                progressDialog.dismiss()
                Toast.makeText(this@DashboardActivity, "Data gagal diperbarui", Toast.LENGTH_SHORT).show()
            }
            override fun onDataChange(dataSnapshot: DataSnapshot)
            {
                progressDialog.dismiss()
                arrayData = ArrayList()
                for(item in dataSnapshot.children){
                    val data0 = item.value as HashMap<*,*>
                    val dataA = data0["permintaan"] as HashMap<*,*>
                    val dataB = data0["pengaduan"] as HashMap<*,*>
                    val data1 = FormObject.MainInfo(data0["key"].toString(), data0["nama"].toString(), data0["seksi"].toString(), data0["subseksi"].toString(), data0["tanggal"].toString(), data0["jenis"].toString(),
                        FormObject.PermintaanInfo(dataA["nama_barang"].toString(), dataA["jumlah_barang"].toString().toInt()),
                        FormObject.PengaduanInfo(dataB["uraian_pengaduan"].toString(), dataB["foto_barang"].toString()))
                    arrayData.add(data1)
                }
                Toast.makeText(this@DashboardActivity, "Data telah diperbarui", Toast.LENGTH_SHORT).show()
                initiateList()
            }
        })
    }

    private fun generateExcel() : Boolean
    {
        val wb = HSSFWorkbook()

        val sheetATK = wb.createSheet("Permintaan ATK")
        val cellStyle = wb.createCellStyle()
        var row : Row
        var cell : Cell

        cellStyle.alignment = HSSFCellStyle.ALIGN_CENTER
        row = sheetATK.createRow(0)
        cell = row.createCell(0)
        cell.setCellValue("No")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(1)
        cell.setCellValue("Nama")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(2)
        cell.setCellValue("Seksi")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(3)
        cell.setCellValue("Subseksi")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(4)
        cell.setCellValue("Tanggal")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(5)
        cell.setCellValue("Nama barang")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(6)
        cell.setCellValue("Jumlah Barang")
        cell.setCellStyle(cellStyle)

        sheetATK.setColumnWidth(0, 5*500)
        sheetATK.setColumnWidth(1, 15*500)
        sheetATK.setColumnWidth(2, 15*500)
        sheetATK.setColumnWidth(3, 15*500)
        sheetATK.setColumnWidth(4, 15*500)
        sheetATK.setColumnWidth(5, 15*500)
        sheetATK.setColumnWidth(6, 15*500)

        var index = 1
        cellStyle.alignment = HSSFCellStyle.ALIGN_LEFT
        for(data in arrayData)
        {
            if(data.jenis == "Permintaan ATK")
            {
                row = sheetATK.createRow(index)
                cell = row.createCell(0)
                cell.setCellValue(index.toString())
                cell.setCellStyle(cellStyle)

                cell = row.createCell(1)
                cell.setCellValue(data.nama)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(2)
                cell.setCellValue(data.seksi)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(3)
                cell.setCellValue(data.subseksi)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(4)
                cell.setCellValue(data.tanggal)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(5)
                cell.setCellValue(data.permintaan.nama_barang)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(6)
                cell.setCellValue(data.permintaan.jumlah_barang.toString())
                cell.setCellStyle(cellStyle)

                index++
            }
        }

        val sheetBMN = wb.createSheet("Pengaduan Masalah BMN")

        cellStyle.alignment = HSSFCellStyle.ALIGN_CENTER
        row = sheetBMN.createRow(0)
        cell = row.createCell(0)
        cell.setCellValue("No")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(1)
        cell.setCellValue("Nama")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(2)
        cell.setCellValue("Seksi")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(3)
        cell.setCellValue("Subseksi")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(4)
        cell.setCellValue("Tanggal")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(5)
        cell.setCellValue("Uraian Pengaduan")
        cell.setCellStyle(cellStyle)
        cell = row.createCell(6)
        cell.setCellValue("Nama File")
        cell.setCellStyle(cellStyle)

        sheetBMN.setColumnWidth(0, 5*500)
        sheetBMN.setColumnWidth(1, 15*500)
        sheetBMN.setColumnWidth(2, 15*500)
        sheetBMN.setColumnWidth(3, 15*500)
        sheetBMN.setColumnWidth(4, 15*500)
        sheetBMN.setColumnWidth(5, 15*500)
        sheetBMN.setColumnWidth(6, 15*500)

        index = 1
        cellStyle.alignment = HSSFCellStyle.ALIGN_LEFT
        for(data in arrayData)
        {
            if(data.jenis == "Pengaduan Masalah BMN")
            {
                row = sheetBMN.createRow(index)
                cell = row.createCell(0)
                cell.setCellValue(index.toString())
                cell.setCellStyle(cellStyle)

                cell = row.createCell(1)
                cell.setCellValue(data.nama)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(2)
                cell.setCellValue(data.seksi)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(3)
                cell.setCellValue(data.subseksi)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(4)
                cell.setCellValue(data.tanggal)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(5)
                cell.setCellValue(data.pengaduan.uraian_pengaduan)
                cell.setCellStyle(cellStyle)

                cell = row.createCell(6)
                cell.setCellValue("${data.key}.jpg")
                cell.setCellStyle(cellStyle)

                index++
            }
        }

        val folderLocation: File? = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        val excelFile = File(folderLocation, excelFileName)
        var os: FileOutputStream? = null
        var success = false

        try
        {
            os = FileOutputStream(excelFile)
            wb.write(os)
            success = true
        }
        catch (e: Exception)
        {
            Log.w("FileUtils", "Failed to save file", e)
        }
        finally
        {
            try
            {
                os?.close()
            }
            catch (ex: Exception) { }
        }

        return success
    }

    inner class ExportToExcel : AsyncTask<String, String, Boolean>()
    {
        private lateinit var dialog : ProgressDialog

        override fun onPreExecute() {
            dialog = ProgressDialog(this@DashboardActivity)
            dialog.setMessage("Sedang mengexport. . .")
            dialog.setCancelable(false)
            dialog.show()
        }

        override fun doInBackground(vararg params: String?): Boolean {
            return generateExcel()
        }

        override fun onPostExecute(result: Boolean) {
            dialog.dismiss()
            if(result) Toast.makeText(this@DashboardActivity, "Export berhasil", Toast.LENGTH_SHORT).show()
            else Toast.makeText(this@DashboardActivity, "Export gagal", Toast.LENGTH_SHORT).show()
        }
    }
}
