package id.go.beacukai.simpelbmn

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity

class SplashscreenActivity : AppCompatActivity()
{
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        val handler = Handler()
        handler.postDelayed(
            {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }, 500L
        )
    }

    override fun onBackPressed() {}
}
