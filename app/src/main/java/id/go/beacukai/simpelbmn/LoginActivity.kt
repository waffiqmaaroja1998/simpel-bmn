package id.go.beacukai.simpelbmn

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var mAuth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance()

        login_sign_user.setOnClickListener {
            val intentForm = Intent(this, FormActivity::class.java)
            startActivity(intentForm)
            this.finish()
        }
        login_sign_admin.setOnClickListener {
            val user = login_username.text.toString()
            val pass = login_password.text.toString()
            checkAuthorization(user, pass)
        }
    }

    private fun checkAuthorization(username : String, password : String)
    {
        if(username.isNotEmpty() && password.isNotEmpty())
        {
            val progressDialog = ProgressDialog(this)
            progressDialog.setMessage("Please wait...")
            progressDialog.setCancelable(false)
            progressDialog.show()

            mAuth.signInWithEmailAndPassword(convertToEmail(username), password)
                .addOnCompleteListener {task ->
                    progressDialog.dismiss()
                    if(task.isSuccessful){
                        val intentForm = Intent(this, DashboardActivity::class.java)
                        startActivity(intentForm)
                        this.finish()
                    }
                    else Toast.makeText(this, "Sign In Failed", Toast.LENGTH_SHORT).show()
                }
                .addOnCanceledListener {
                    progressDialog.dismiss()
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Sign In Failed", Toast.LENGTH_SHORT).show()
                }
        }
    }

    private fun convertToEmail(username : String) : String
    {
        return "$username@admin.bmn"
    }
}
