package id.go.beacukai.simpelbmn.DatabaseCollection

class FormObject
{
    data class MainInfo(
        val key : String,
        val nama : String,
        val seksi : String,
        val subseksi : String,
        val tanggal : String,
        val jenis : String,
        val permintaan : PermintaanInfo,
        val pengaduan : PengaduanInfo
    )

    data class PermintaanInfo(
        val nama_barang : String,
        val jumlah_barang : Int
    )

    data class PengaduanInfo(
        val uraian_pengaduan : String,
        val foto_barang : String
    )
}