package id.go.beacukai.simpelbmn

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import id.go.beacukai.simpelbmn.DatabaseCollection.FormObject
import kotlinx.android.synthetic.main.activity_form.*
import java.io.*
import java.nio.channels.FileChannel
import java.text.SimpleDateFormat
import java.util.*

class FormActivity : AppCompatActivity()
{

    private val PERMISSION_CODE_GALLERY = 1000
    private val GALLERY_REQUEST_CODE = 1001
    private val MAX_HEIGHT = 1024
    private val MAX_WIDTH = 1024
    private var imageReady: File? = null
    private var imageTemp: File? = null
    private var imageUri : Uri? = null

    private var nameThis = ""
    private var seksiThis = ""
    private var subseksiThis = ""
    private var tanggalThis = ""
    private var jenisThis = ""
    private var namaBarangThis = ""
    private var jumlahBarangThis = 0
    private var pengaduanThis = ""
    private var fotoThis = ""

    private lateinit var _calendar: Calendar
    private lateinit var _datePicker: DatePickerDialog.OnDateSetListener
    private lateinit var database: DatabaseReference
    private lateinit var storage: StorageReference

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)

        database = FirebaseDatabase.getInstance().reference
        storage = FirebaseStorage.getInstance().reference

        initiateEmpty()
        menuSetting()
    }

    private fun initiateEmpty()
    {
        nameThis = ""
        seksiThis = ""
        subseksiThis = ""
        tanggalThis = ""
        jenisThis = ""
        namaBarangThis = ""
        jumlahBarangThis = 0
        pengaduanThis = ""
        fotoThis = ""
        imageReady = null
        imageTemp = null
        imageUri = null

        form_edit_name.setText("")
        form_spinner_seksi.setSelection(0)
        form_spinner_subseksi.setSelection(0)
        form_txt_date.setText("")
        form_atk_name.setText("")
        form_atk_jumlah.setText("")
        form_bmn_pengaduan.setText("")
        setDefaultImage()
        whichShowedMenu("")
    }

    private fun whichShowedMenu(jenis: String)
    {
        when (jenis)
        {
            "Permintaan ATK" ->
            {
                form_atk_layout.visibility = View.VISIBLE
                form_bmn_layout.visibility = View.GONE
            }
            "Pengaduan Masalah BMN" ->
            {
                form_atk_layout.visibility = View.GONE
                form_bmn_layout.visibility = View.VISIBLE
            }
            else ->
            {
                form_atk_layout.visibility = View.GONE
                form_bmn_layout.visibility = View.GONE
            }
        }
    }

    private fun setDefaultImage()
    {
        fotoThis = ""
        form_bmn_foto.setImageDrawable(resources.getDrawable(R.drawable.image_default, applicationContext.theme))
    }

    private fun menuSetting()
    {
        _calendar = Calendar.getInstance()
        _datePicker = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            _calendar.set(Calendar.YEAR, year)
            _calendar.set(Calendar.MONTH, monthOfYear)
            _calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val newFormat = "dd-MM-yyyy"
            val dateFormat = SimpleDateFormat(newFormat)
            form_txt_date.setText(dateFormat.format(_calendar.time))
        }
        form_txt_date.setOnClickListener {
            DatePickerDialog(
                this, _datePicker,
                _calendar.get(Calendar.YEAR), _calendar.get(Calendar.MONTH), _calendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        form_spinner_seksi.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
        {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                val adapter = when(position)
                {
                    1 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_0, android.R.layout.simple_spinner_item)
                    2 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_1, android.R.layout.simple_spinner_item)
                    3 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_2, android.R.layout.simple_spinner_item)
                    4 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_3, android.R.layout.simple_spinner_item)
                    5 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_4, android.R.layout.simple_spinner_item)
                    6 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_5, android.R.layout.simple_spinner_item)
                    7 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_6, android.R.layout.simple_spinner_item)
                    8 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_7, android.R.layout.simple_spinner_item)
                    9 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_8, android.R.layout.simple_spinner_item)
                    10 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_9, android.R.layout.simple_spinner_item)
                    11 -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_10, android.R.layout.simple_spinner_item)
                    else -> ArrayAdapter.createFromResource(this@FormActivity, R.array.subseksi_000, android.R.layout.simple_spinner_item)
                }
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                form_spinner_subseksi.adapter = adapter
            }

            override fun onNothingSelected(p0: AdapterView<*>?)
            {
                TODO("not yet implemented")
            }
        }

        form_spinner_action.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
        {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                val str = form_spinner_action.getItemAtPosition(position).toString()
                whichShowedMenu(str)
            }

            override fun onNothingSelected(p0: AdapterView<*>?)
            {
                TODO("not yet implemented")
            }
        }

        form_bmn_pick_foto.setOnClickListener {
            selectImageGallery()
        }

        form_reset.setOnClickListener {
            val alertDialog = AlertDialog.Builder(this, R.style.AlertDialogCustom)
            alertDialog
                .setTitle("Hapus Data")
                .setMessage("Data yang telah dimasukkan akan diatur ulang.\nApakah anda yakin?")
                .setPositiveButton("Ya"){_,_ ->
                    initiateEmpty()
                }
                .setNegativeButton("Tidak"){_,_ -> }
                .setCancelable(false)
            val dialog = alertDialog.create()
            dialog.show()
        }

        form_upload.setOnClickListener {
            val alertDialog = AlertDialog.Builder(this, R.style.AlertDialogCustom)
            alertDialog
                .setTitle("Kirim Data")
                .setMessage("Apakah anda ingin mengirim laporan sekarang?")
                .setPositiveButton("Ya"){_,_ ->
                    sendData()
                }
                .setNegativeButton("Nanti"){_,_ -> }
                .setCancelable(false)
            val dialog = alertDialog.create()
            dialog.show()
        }
    }

    //<editor-fold desc="Pilih Foto">
    private fun selectImageGallery()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
            )
            {
                val permission =
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                requestPermissions(permission, PERMISSION_CODE_GALLERY)
            }
            else
            {
                dispatchPickPictureIntent()
            }
        }
        else
        {
            dispatchPickPictureIntent()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    {
        when (requestCode)
        {
            PERMISSION_CODE_GALLERY ->
            {
                if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                {
                    dispatchPickPictureIntent()
                }
                else
                {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun getImageFile(): File
    {
        val timestamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val pictureFileName = "bmn$timestamp"
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(pictureFileName, ".jpg", storageDir)
    }

    private fun dispatchPickPictureIntent()
    {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)

        var imgFile: File? = null
        try
        {
            imgFile = getImageFile()
            imageTemp = imgFile
        }
        catch (e: IOException)
        {
        }
        if (imgFile != null)
            startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }
    //</editor-fold>

    //<editor-fold desc="Image Processing">
    @Throws(IOException::class)
    fun handleSamplingAndRotationBitmap(context: Context, selectedImage: Uri): Bitmap
    {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var imageStream = context.contentResolver.openInputStream(selectedImage)
        BitmapFactory.decodeStream(imageStream, null, options)
        imageStream?.close()
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(
            options,
            MAX_WIDTH,
            MAX_HEIGHT
        )
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        imageStream = context.contentResolver.openInputStream(selectedImage)
        var img = BitmapFactory.decodeStream(imageStream, null, options)
        img = rotateImageIfRequired(img, selectedImage)
        //Compress bitmap
        try
        {
            val output = FileOutputStream(imageReady)
            img.compress(Bitmap.CompressFormat.JPEG, 80, output)
        }
        catch (e: FileNotFoundException)
        {
            e.printStackTrace()
        }
        return img
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int
    {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth)
        {
            // Calculate ratios of height and width to requested height and width
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).
            val totalPixels = (width * height).toFloat()
            // Anything more than 2x the requested pixels we'll sample down further
            val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap)
            {
                inSampleSize++
            }
        }
        return inSampleSize
    }

    @Throws(IOException::class)
    private fun rotateImageIfRequired(img: Bitmap, selectedImage: Uri): Bitmap
    {
        val ei = ExifInterface(selectedImage.path)
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        return when (orientation)
        {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(img, 90.0F)
            ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(img, 180.0F)
            ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(img, 270.0F)
            else -> img
        }
    }

    private fun rotateImage(img: Bitmap, angle: Float): Bitmap
    {
        val matrix = Matrix()
        matrix.postRotate(angle)
        val rotatedImage = Bitmap.createBitmap(img, 0, 0, img.width, img.height, matrix, true)
        img.recycle()
        return rotatedImage
    }

    /**
     * copies content from source file to destination
     *
     * @param sourceFile
     * @param destFile
     */
    @Throws(IOException::class, FileNotFoundException::class)
    private fun copyFile(sourceFile: File, destFile: File?)
    {
        if (!sourceFile.exists()) return
        val source: FileChannel? = FileInputStream(sourceFile).channel
        val dest: FileChannel? = FileOutputStream(destFile).channel
        if (dest != null && source != null)
        {
            dest.transferFrom(source, 0, source.size())
        }
        source?.close()
        dest?.close()
    }
    //</editor-fold>

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
        {
            when (requestCode)
            {
                GALLERY_REQUEST_CODE ->
                {
                    imageReady?.delete()
                    imageReady = imageTemp
                    imageTemp = null
                    val selectedImage = data?.data
                    val filePathColumn: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = contentResolver.query(selectedImage!!, filePathColumn, null, null, null)
                    cursor?.moveToFirst()
                    val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
                    val source = File(cursor?.getString(columnIndex!!))
                    cursor?.close()
                    val dest = imageReady
                    copyFile(source, dest)
                    if (dest != null)
                    {
                        val photoUri = Uri.fromFile(dest)
                        imageUri = photoUri
                        val processedImage = handleSamplingAndRotationBitmap(this, photoUri)
                        form_bmn_foto.setImageBitmap(processedImage)
                    }
                }
            }
        }
    }

    private fun validateSpinnerSelectedItem() : Boolean
    {
        return !(seksiThis == "--Pilih Seksi Terkait--" || subseksiThis == "--Pilih Subseksi Terkait--")
    }

    private fun sendData()
    {
        nameThis = form_edit_name.text.toString()
        seksiThis = form_spinner_seksi.selectedItem.toString()
        subseksiThis = form_spinner_subseksi.selectedItem.toString()
        tanggalThis = form_txt_date.text.toString()
        val validSpinner = validateSpinnerSelectedItem()
        if (nameThis.isNotEmpty() && validSpinner && tanggalThis.isNotEmpty())
        {
            when (form_spinner_action.selectedItemPosition)
            {
                1 ->
                {
                    jenisThis = form_spinner_action.selectedItem.toString()
                    namaBarangThis = form_atk_name.text.toString()
                    jumlahBarangThis = try
                    {
                        val temp = form_atk_jumlah.text.toString()
                        if (temp.isEmpty()) 0 else temp.toInt()
                    }
                    catch (e: Exception)
                    {
                        0
                    }
                    if (namaBarangThis.isNotEmpty() && jumlahBarangThis > 0)
                    {
                        pengaduanThis = ""
                        fotoThis = ""
                        val key = database.child("data").push().key ?: "none"
                        val dataATK = FormObject.PermintaanInfo(namaBarangThis, jumlahBarangThis)
                        val dataBMN = FormObject.PengaduanInfo(pengaduanThis, fotoThis)
                        val data =
                            FormObject.MainInfo(key, nameThis, seksiThis, subseksiThis, tanggalThis, jenisThis, dataATK, dataBMN)
                        writeNewData(data)
                    }
                    else Toast.makeText(this, "Data pengajuan belum benar atau belum lengkap", Toast.LENGTH_SHORT).show()
                }
                2 ->
                {
                    jenisThis = form_spinner_action.selectedItem.toString()
                    pengaduanThis = form_bmn_pengaduan.text.toString()
                    fotoThis = imageReady?.name ?: ""
                    if (pengaduanThis.isNotEmpty() && fotoThis.isNotEmpty())
                    {
                        namaBarangThis = ""
                        jumlahBarangThis = 0
                        val key = database.child("data").push().key ?: "none"
                        val dataATK = FormObject.PermintaanInfo(namaBarangThis, jumlahBarangThis)
                        val dataBMN = FormObject.PengaduanInfo(pengaduanThis, fotoThis)
                        val data =
                            FormObject.MainInfo(key, nameThis, seksiThis, subseksiThis, tanggalThis, jenisThis, dataATK, dataBMN)
                        uploadImage(data)
                    }
                    else Toast.makeText(this, "Data pengajuan belum benar atau belum lengkap", Toast.LENGTH_SHORT).show()
                }
                else -> Toast.makeText(this, "Belum memilih Jenis Pengaduan", Toast.LENGTH_SHORT).show()
            }
        }
        else Toast.makeText(this, "Data utama belum benar atau belum lengkap", Toast.LENGTH_SHORT).show()
    }

    private fun uploadImage(data: FormObject.MainInfo)
    {
        if(imageUri != null && data.key != "none")
        {
            val progressDialog = ProgressDialog(this)
            progressDialog.setMessage("Uploading...")
            progressDialog.setCancelable(false)
            progressDialog.show()

            val ref = storage.child("images/${data.key}.jpg")
            ref.putFile(imageUri!!)
                .addOnSuccessListener {
                    progressDialog.dismiss()
                    writeNewData(data)
                }
                .addOnFailureListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Data gagal dikirim", Toast.LENGTH_SHORT).show()
                }
                .addOnCanceledListener {
                    progressDialog.dismiss()
                    Toast.makeText(this, "Data gagal dikirim", Toast.LENGTH_SHORT).show()
                }
                .addOnProgressListener {taskSnapshot ->
                    val progress = (100.0*taskSnapshot.bytesTransferred/taskSnapshot.totalByteCount)
                    progressDialog.setMessage("Uploaded : $progress%")
                }
        }
        else Toast.makeText(this, "Data gagal dikirim", Toast.LENGTH_SHORT).show()
    }

    private fun writeNewData(data: FormObject.MainInfo)
    {
        when(data.key)
        {
            "none" -> Toast.makeText(this, "Data gagal dikirim", Toast.LENGTH_SHORT).show()
            else ->
            {
                val progressDialog = ProgressDialog(this)
                progressDialog.setMessage("Uploading...")
                progressDialog.setCancelable(false)
                progressDialog.show()

                database.child("data").child(data.key).setValue(data)
                    .addOnSuccessListener {
                        progressDialog.dismiss()
                        imageReady?.delete()
                        initiateEmpty()
                        Toast.makeText(this, "Data telah terkirim", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        progressDialog.dismiss()
                        Toast.makeText(this, "Data gagal dikirim", Toast.LENGTH_SHORT).show()
                    }
                    .addOnCanceledListener {
                        progressDialog.dismiss()
                        Toast.makeText(this, "Data gagal dikirim", Toast.LENGTH_SHORT).show()
                    }
            }
        }
    }

    override fun onBackPressed()
    {
        this.finish()
    }
}
